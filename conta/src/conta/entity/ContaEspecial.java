package conta.entity;

public class ContaEspecial extends Conta {

	protected double limite;
	
	public double getLimite() {
		return this.limite;
	}
	
	public void setLimite(double limite) {
		if (limite > 0)
			this.limite = limite;
	}
	
	@Override
	public void debitar(double valor) {
		if ((this.saldo + this.limite) >= valor)
			this.saldo -= valor;
	}	


}
