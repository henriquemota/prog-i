package conta.entity;

import java.util.ArrayList;
import java.util.List;

public class Cliente extends Base {
	
	public Cliente() {
		this.Contas = new ArrayList<Conta>();
	}

	protected String CPF;
	protected String Nome;	
	protected List<Conta> Contas;
	
	public String getCPF() {return this.CPF;}
	public void setCPF(String cpf) {this.CPF = cpf;}
	
	public String getNome() {return this.Nome;}
	public void setNome(String nome) {this.Nome = nome;}
	
	public List<Conta> getContas() { return this.Contas; }
	
	public void setConta(Conta conta) {
		this.Contas.add(conta);
	}
	
}
