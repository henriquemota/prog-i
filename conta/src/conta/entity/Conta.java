package conta.entity;

public class Conta extends Base {
	
	protected int agencia;
	protected int conta;
	protected String digito;
	protected double saldo;
	
	public int getAgencia() {
		return this.agencia;
	}
	
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	
	public int getConta() {
		return this.conta;
	}
	
	public void setConta(int conta) {
		this.conta = conta;
	}
	
	public String getDigito() {
		this.digito = String.valueOf((this.conta + this.agencia) % 2);
		return this.digito;
	}
	
	public double getSaldo() {
		return this.saldo;
	}
	
	public void creditar(double valor) {
		if (valor > 0)
			this.saldo += valor;
	}
	
	public void debitar(double valor) {
		if (this.saldo >= valor)
			this.saldo -= valor;
	}	

}
