package conta.view;

import java.util.Scanner;

public class Menu extends Base{

	private Scanner scan;
	private TelaCliente telaCliente;

	public void principal() {
		scan = new Scanner(System.in);
		String opcao = "";
		while(!opcao.equals("f")) {
			System.out.println("Informe a opção desejada");
			System.out.println("1. Registrar novo cliente");
			System.out.println("2. Consultar cliente existente");
			System.out.println("3. Registrar nova conta para cliente");
			System.out.println("4. Realizar operação de crédito");
			System.out.println("f. Encerra o programa");
			opcao = scan.nextLine();
			
			switch (opcao) {
			case "1":
				telaCliente = new TelaCliente();
				telaCliente.novoCliente();
				break;
			case "2":
				telaCliente = new TelaCliente();
				telaCliente.buscarCliente();
				break;
			case "3":
				break;
			case "4":
				break;
			}
		}
	}
	
}
