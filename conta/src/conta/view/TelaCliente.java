package conta.view;

import java.util.Scanner;
import conta.entity.Cliente;

public class TelaCliente extends Base
{
	
	private Scanner scan;

	public void novoCliente() {
		scan = new Scanner(System.in);

		Cliente cliente = new Cliente();
		
		System.out.print("\nInforme o CPF do cliente: ");
		String cpf = scan.nextLine();
		cliente.setCPF(cpf);
		
		System.out.print("\nInforme o nome do cliente: ");
		String nome = scan.nextLine();
		cliente.setNome(nome);
		
		Base.clientes.add(cliente);

		System.out.print("\nCliente registrado com sucesso. ");
		
	}
	
	public void buscarCliente() {
		scan = new Scanner(System.in);

		System.out.print("\nInforme o CPF do cliente: ");
		String cpf = scan.nextLine();
		
		for (Cliente c : Base.clientes) {
			if (c.getCPF().equals(cpf)) {
				Base.cliente = c;
				System.out.print("\nCliente " + c.getNome() + " localizado ");
			}
		}
		if (Base.cliente == null) System.out.print("\nCliente não localizado ");		
	}

}
