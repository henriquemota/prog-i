package academico.views;

import academico.models.*;

public class Academico {

	public static void main(String[] args) {
		
		//Professor p1 = new Professor(1, "Henrique", Titulacao.MESTRE);
		//Disciplina d1 = new Disciplina(1, "Programação I", 4);
		Turma t1 = new Turma(Horario.CD_NOITE, new Disciplina(1, "Programação I", 4), new Professor(1, "Henrique", Titulacao.MESTRE));
		
		for (int i = 1; i <= 20; i++) {
			t1.matricular(new Aluno(i, "Aluno " + i, "2019" + i));
			System.out.println(t1.getMatriculas().size() + " alunos matriculados na disciplina " + t1.getDisciplina().getNome());
		}
		
		Aluno a = t1.alunoEstaMatriculado("201910");

		if (a != null)
			System.out.println(a.getNome() + " - " + a.getMatricula());
		else
			System.out.println("Aluno não encontrado");
		
		a = null;
		a = t1.alunoEstaMatriculado("201950");
		
		if (a != null)
			System.out.println(a.getNome() + " - " + a.getMatricula());
		else
			System.out.println("Aluno não encontrado");
	}

}
