package academico.models;

import java.util.ArrayList;
import java.util.List;

public class Turma extends Base {
	
	public Turma(Horario horario, Disciplina disciplina, Professor professor) {
		this.horario = horario;
		this.professor = professor;
		this.disciplina = disciplina;
		this.matriculas = new ArrayList<Aluno>();
	}
	
	private Horario horario;
	private Professor professor;
	private Disciplina disciplina;
	private ArrayList<Aluno> matriculas;
	
	public Professor getProfessor() {
		return this.professor;
	}
	
	public Horario getHorario() {
		return this.horario;
	}
	
	public Disciplina getDisciplina() {
		return this.disciplina;
	}
	
	public void matricular(Aluno aluno) {
		this.matriculas.add(aluno);
	}
	
	public List<Aluno> getMatriculas() {
		return this.matriculas;
	}
	
	public Aluno alunoEstaMatriculado(String matricula) {
		for (Aluno aluno : matriculas) {
			if (aluno.getMatricula().equals(matricula))
				return aluno;
		}
		return null;
	}
	
}
