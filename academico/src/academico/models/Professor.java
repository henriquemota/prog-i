package academico.models;

public class Professor extends Base {
	
	public Professor() {}
	
	public Professor(int id, String nome, Titulacao titulacao) {
		super.id = id;
		this.nome = nome;
		this.titulacao = titulacao;
	}

	private String nome;
	private Titulacao titulacao;
	
	public String getNome() {
		return this.nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Titulacao getTitulacao() {
		return this.titulacao;
	}
	
	public void setTitulacao(Titulacao titulacao) {
		this.titulacao = titulacao;
	}

}
