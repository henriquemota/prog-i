package academico.models;

public class Disciplina extends Base {
	
	public Disciplina() {}
	
	public Disciplina(int id, String nome, int creditos) {
		super.id = id;
		this.nome = nome;
		this.creditos = this.checarCreditos(creditos);
	}
	
	private String nome;
	private int creditos;
		
	public String getNome() {
		return this.nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getCreditos() {
		return this.creditos;
	}
	
	public void setCreditos(int creditos) {
		this.creditos = this.checarCreditos(creditos);
	}
	
	private int checarCreditos(int creditos) {
		if (creditos < 2) return 2;
		if (creditos > 8) return 8;
		return creditos;
	}


}
