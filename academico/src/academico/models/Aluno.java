package academico.models;

public class Aluno extends Base {
	
	public Aluno() {}
	
	public Aluno(int id, String nome, String matricula) {
		super.id = id;
		this.nome = nome;
		this.matricula = matricula;
	}

	private String matricula;
	private String nome;
	
	public String getMatricula() {
		return this.matricula;
	}
	
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
