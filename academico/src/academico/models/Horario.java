package academico.models;

public enum Horario {
	AB_MANHA,
	CD_MANHA,
	AB_TARDE,
	CD_TARDE,
	AB_NOITE,
	CD_NOITE
}
