package academico.models;

public enum Titulacao {
	GRADUADO, 
	ESPECIALISTA, 
	MESTRE, 
	DOUTOR, 
	POS_DOUTOR 
}
