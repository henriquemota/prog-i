package lista;

import java.util.Scanner;

public class Lista {

	public static void main(String[] args) {
		menu();
	}
	
	private static void menu() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Informe a opção desejada");
		String opcao = "";
		while (!opcao.toLowerCase().equals("f")) {
			System.out.println("3. Exercício 03");
			System.out.println("F. Fim");
			opcao = scan.next();
			seleciona(opcao);
		}
	}
	
	private static void seleciona(String opcao) {
		switch (opcao) {
			case "3":
				Ex03.calcular();
				break;
		}
	}

}
