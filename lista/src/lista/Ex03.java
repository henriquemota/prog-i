package lista;

import java.util.Scanner;

public class Ex03 {
	
	final static double distribuidor = 0.28;
	final static double impostos = 0.45;

	public static void calcular() {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Informe o valor do custo de fábrica: ");
		double valor = scan.nextDouble();
		
		System.out.println("Valor de fábrica: " + valor);
		System.out.println("Total de impostos: " + getCustoImpostos(valor));
		System.out.println("Total distribuidor: " + getCustoDistribuidor(valor));
		System.out.println("Total consumidor: " + getCustoConsumidor(valor));
	}
	
	private static double getCustoImpostos(double valor) {
		return (valor * impostos);
	}

	private static double getCustoDistribuidor(double valor) {
		return (valor * distribuidor);
	}

	private static double getCustoConsumidor(double valor) {
		return valor + getCustoDistribuidor(valor) + getCustoImpostos(valor);
	}
	
}
