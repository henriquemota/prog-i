package calculadora;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Util {

	private static String popular(String c, int t) {
		String r = "";
		for (int i = 0; i < t; i++) {
			r += c;
		}
		return r;
	}
	
	public static void montarTela() {
		System.out.println(Util.popular("#", 20) + " Calculadora " + Util.popular("#", 20) + "\n");
		System.out.println("Informe a opção desejada");
		System.out.println("1. Soma");
		System.out.println("2. Subtração");
		System.out.println("3. Multiplicação");
		System.out.println("4. Divisão");
		
	}
	
	public static void processar() {
		Scanner s = new Scanner(System.in);
		String opcao = s.nextLine();
		double p1 = 0.0D, p2 = 0.0D;
		String[] menu = {"1", "2", "3", "4"};
		List<String> list = Arrays.asList(menu);
		
		if(!list.contains(opcao)){ 
			System.out.println("Opção inválida");
		} else {
			System.out.print("Informe o primeiro valor: ");
			p1 = s.nextDouble(); 
			System.out.print("Informe o segundo valor: ");
			p2 = s.nextDouble();
			if (opcao.equals("1")) {
				System.out.println("A soma é " + (p1+p2));
			}
			else if (opcao.equals("2")) {
				System.out.println("A subtração é " + (p1-p2));
			}
			else if (opcao.equals("3")) {
				System.out.println("A multiplicação é " + (p1*p2));
			}
			else if (opcao.equals("4")) {
				System.out.println("A divisão é " + (p1/p2));
			}
		}
	}
	
}
