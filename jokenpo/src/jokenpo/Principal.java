package jokenpo;

import javax.swing.JOptionPane;

import jokenpo.entity.*;;

public class Principal {

	public static void main(String[] args) {
		
		Boolean condicaoDeParada = false;
		Jokenpo jogo = new Jokenpo();
		
		while (!condicaoDeParada) {
			String res = JOptionPane.showInputDialog(null, jogo.exibirMenu(), "Jokenpo", JOptionPane.QUESTION_MESSAGE);
			
			//verifica se e um numero 
			if (res == null ||  !res.matches("-?\\d+(\\.\\d+)?")) {
				condicaoDeParada = true;
				break;
			}

			//processa o jogo
			jogo.informarJogadaJogador(Integer.parseInt(res));
			jogo.sortearJogadaComputador();
			JOptionPane.showMessageDialog(null,  jogo.exibirResultado(), "Jokenpo - Resultado", JOptionPane.INFORMATION_MESSAGE);
			
			//exibir o placar
			JOptionPane.showMessageDialog(null,  jogo.exibirPlacar(), "Jokenpo - Placar", JOptionPane.INFORMATION_MESSAGE);
			
			//verifica se o usuario quer jogar novamente
			if (JOptionPane.showConfirmDialog(null, "Deseja jogar novamente?", "Atenção", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null) == 1)
				condicaoDeParada = true;
		}
		
	}

}
