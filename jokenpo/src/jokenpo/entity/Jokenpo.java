package jokenpo.entity;

import java.util.Random;

public class Jokenpo {
	
	int jogador1;
	int jogador2;
	int totalJogador1;
	int totalJogador2;
	Random rnd;
	
	public Jokenpo() {
		this.rnd = new Random();
		this.totalJogador1 = 0;
		this.totalJogador2 = 0;
	}
	
	public String exibirMenu() {
		return new StringBuilder()
		           .append("Bem vindo ao Jokenpo!\n")
		           .append("Informe a opção desejada\n")
		           .append("1 - Pedra\n")
		           .append("2 - Papel\n")
		           .append("3 - Tesoura\n")
		           .toString();
	}
	
	public void informarJogadaJogador(int jogada) {
		this.jogador1 = jogada;
	}
	
	public void sortearJogadaComputador() {
		this.jogador2 = this.rnd.nextInt(3) + 1;
	}
	
	public String exibirResultado() {
		if (this.jogador1 < 1 || this.jogador1 > 3) return "Você escolheu uma opção inválida";
		if (this.jogador1 == this.jogador2) return "Empate com o jogo " + this.exibirJogadaComputador();
	    else if (this.jogador1 == 1 && this.jogador2 != 2) {
	    	this.totalJogador1 +=1;
	    	return "Você venceu, computador jogou " + this.exibirJogadaComputador(); 
    	}
	    else if (this.jogador1 == 2 && this.jogador2 != 3) {
	    	this.totalJogador1 +=1;
	    	return "Você venceu, computador jogou " + this.exibirJogadaComputador();
    	}
	    else if (this.jogador1 == 3 && this.jogador2 != 1) {
	    	this.totalJogador1 +=1;
	    	return "Você venceu, computador jogou " + this.exibirJogadaComputador();
	    }
	    else {
	    	this.totalJogador2 +=1;
	    	return "Computador vence, jogou " + this.exibirJogadaComputador();
	    }
	}
	
	public String exibirPlacar() {
		return new StringBuilder()
		           .append("Placar do jogo!\n")
		           .append("Jogador 1: " + this.totalJogador1 + "\n")
		           .append("Jogador 2: " + this.totalJogador2 + "\n")
		           .toString();
		
	}
	
	private String exibirJogadaComputador() {
		switch (this.jogador2) {
			case 1:
				return "Pedra";
			case 2:
				return "Papel";
			case 3:
				return "Tesoura";
		}
		return "Nao selecionado";
	}	

}
