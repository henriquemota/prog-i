package oo.classes;

public class Conta extends Base {
	
	public Conta() {}

	public Conta(int agencia, int numero, double saldo) {
		this.agencia = agencia;
		this.numero = numero;
		this.saldo = saldo;
	}

	private int agencia;
	private int numero;
	private double saldo;
	
	public int getAgencia() {return this.agencia;}
	public void setAgencia(int valor) {this.agencia = valor;}
	
	public int getNumero() {return this.numero;}
	public void setNumero(int valor) {this.numero = valor;}
	
	public double getSaldo() { return this.saldo; }
	
	public boolean creditar(double valor) {
		this.saldo += valor;
		return true;
	}
	
	public boolean debitar(double valor) {
		if (this.saldo >= valor) {
			this.saldo -= valor;
			return true;
		} 
		return false;
	}

	
}
