package oo;

import oo.classes.*;

public class Principal {

	public static void main(String[] args) {
		Conta joao = new Conta(100, 12345, 1000);
		
		//joao.setAgencia(1000);
		//joao.setNumero(12345);
		
		joao.creditar(1000);
		
		System.out.println("Saldo da conta " + joao.getNumero() + " é " + joao.getSaldo());
		
		if (joao.debitar(900)) { 
			System.out.println("Operação realizada"); 
		} else {
			System.out.println("Operação não realizada");
		}
		
		System.out.println("Saldo da conta " + joao.getNumero() + " é " + joao.getSaldo());
		
		if (joao.debitar(1000)) { 
			System.out.println("Operação realizada"); 
		} else {
			System.out.println("Operação não realizada");
		}
		
		System.out.println("Saldo da conta " + joao.getNumero() + " é " + joao.getSaldo());

	}

}
