package forca;

import java.util.Scanner;

public class Forca {
	
	static String sorteada;
	static String digitadas;
	static int erros;
	static boolean venceu;
	
	public static void main(String[] args) {

		digitadas = new String();
		sorteada = sortearPalavra();
		Scanner scan = new Scanner(System.in);
		
		while(erros < 6 && !venceu) {
			clearConsole();
			
			System.out.println(imprimirTabuleiro(erros));
			System.out.println("\nDescubra a palavra com " + sorteada.length() + " letras\n");
			System.out.println(montarResultado());
			
			String[] d = digitadas.trim().length() > 0 ? digitadas.split("") : new String[0];
			if (d.length > 0) {
				System.out.println("\nAs palavras digitadas até o momento foram\n");
				for (int i = 0; i < d.length; i++)  System.out.print(d[i] + " ");
				System.out.print("\n");
			}
			
			System.out.println("\nInforme uma letra\n");
			gravarDigitada(scan.next().toLowerCase());
			
			if (!montarResultado().contains("_")) venceu = true;
		}
		scan.close();
		
		clearConsole();
		System.out.println(imprimirTabuleiro(erros));
		System.out.println("Fim de jogo, você " + (venceu ? "venceu" : "perdeu") + " - palavra: " + sorteada);
		
	}
	
	private static String imprimirTabuleiro(int num) {
		String[] tabuleiro = new String[7];
		tabuleiro[0] = ""
		+ " +-----+\n"
		+ "       |\n"
		+ "       |\n"
		+ "       |\n"
		+ "       |\n"
		+ " ------+ ";
		tabuleiro[1] = ""
		+ " +-----+\n"
		+ " O     |\n"
		+ "       |\n"
		+ "       |\n"
		+ "       |\n"
		+ " ------+ ";
		tabuleiro[2] = ""
		+ " +-----+\n"
		+ " O     |\n"
		+ " |     |\n"
		+ "       |\n"
		+ "       |\n"
		+ " ------+ ";
		tabuleiro[3] = ""
		+ " +-----+\n"
		+ " O     |\n"
		+ "/|     |\n"
		+ "       |\n"
		+ "       |\n"
		+ " ------+ ";
		tabuleiro[4] = ""
		+ " +-----+\n"
		+ " O     |\n"
		+ "/|\\    |\n"
		+ "       |\n"
		+ "       |\n"
		+ " ------+ ";
		tabuleiro[5] = ""
		+ " +-----+\n"
		+ " O     |\n"
		+ "/|\\    |\n"
		+ "/      |\n"
		+ "       |\n"
		+ " ------+ ";
		tabuleiro[6] = ""
		+ " +-----+\n"
		+ " O     |\n"
		+ "/|\\    |\n"
		+ "/ \\    |\n"
		+ "       |\n"
		+ " ------+ ";
		
		if (num < 0) num = 0;
		if (num > 6) num = 6;
		
		return tabuleiro[num];
	}

	private static String sortearPalavra() {
		String[] palavras = {"casa", "amor", "java", "sopa", "carro", "bla"};
		int k  = (int) Math.floor(Math.random() * palavras.length);
		return palavras[k];
	}
	
	private static String montarResultado() {
		String[] s = sorteada.split("");
		String retorno = new String();
		for (int i = 0; i < s.length; i++)  retorno += retornaCaractere(s[i].trim()) + " "; 
		return retorno;
	}

	private static String retornaCaractere(String item) {
		String[] d = digitadas.split("");
		for (int i = 0; i < d.length; i++) 
			if (item.equals(d[i].trim())) 
				return d[i].trim();

		return "_";
	} 
	
	private static void gravarDigitada(String letra) {
		if (!digitadas.contains(letra)) digitadas += letra;
		
		erros = 0;
		String[] d = digitadas.trim().length() > 0 ? digitadas.split("") : new String[0];
		for (int i = 0; i < d.length; i++) 
			if (!sorteada.contains(d[i])) erros ++;
	}

	private static void clearConsole()
	{
		
	}
	
}
